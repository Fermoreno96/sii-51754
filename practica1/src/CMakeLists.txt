INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS_CLIENTE
	MundoCliente.cpp
	Esfera.cpp
	Plano.cpp	
	Raqueta.cpp	
	Vector2D.cpp)

SET(COMMON_SRCS_SERVIDOR
	MundoServidor.cpp
	Esfera.cpp
	Plano.cpp	
	Raqueta.cpp	
	Vector2D.cpp)
	
ADD_EXECUTABLE(logger Logger.cpp)
ADD_EXECUTABLE(bot Bot.cpp)
ADD_EXECUTABLE(cliente Cliente.cpp ${COMMON_SRCS_CLIENTE}) 
ADD_EXECUTABLE(servidor Servidor.cpp ${COMMON_SRCS_SERVIDOR}) 

TARGET_LINK_LIBRARIES(cliente glut GL GLU -lpthread)
TARGET_LINK_LIBRARIES(servidor glut GL GLU -lpthread)

